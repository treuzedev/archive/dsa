usage:
	@echo "Usage:"
	@echo "make usage (default)"
	@echo "make all_tests"
	@echo "make all_tests_with_output"
	@echo "make test TEST=<name_of_test>"
	@echo "make test_with_output TEST=<name_of_test>"
	@echo "make full_coverage"
	@echo "make single_coverage TEST=<name_of_test>"

all_tests:
	@pytest

all_tests_with_output:
	@pytest -s

test:
	@pytest tests/test_${TEST}.py

test_with_output:
	@pytest -s tests/test_${TEST}.py

full_coverage:
	@coverage run -m pytest
	@coverage report
	@coverage xml

single_coverage:
	@coverage run -m pytest tests/test_${TEST}.py
	@coverage report
	@coverage xml
