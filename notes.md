## General workflow

- Pseudocode the problem; inputs and outputs
- Generate test cases (do not forget edge cases)
- Set up logging while developing
- Functions should have 7-8, max 10 lines (maximum amount of code we can hold in our memory)
- Analyze space and time inneficiencies and fix them
- Make it Python friendly: __setitem__, __iter__, etc..

<br>

## Notes

##### Complexity

Complexity: amount of time and space necessary to process an input of a given size in the worst case scenario

<br>

##### Binary search tree

For example, a class representing a user, and a class representing a database.

The user has user attributes, and the database holds the users via attributes and we work with the database via methods (adding user, removing user, etc..).

A node has 0, 1 or 2 child nodes.

A node can have only keys, or keys and values (ie, a map).

Height of a tree:
  height=k (starting from 0)
  number_of_nodes = 2^(k-1)
  height needed: k = log(n+1), where n is the number of records

Balanced BST:
  insert, find and update have complexity of log(n) (a list would have complexity n)

Inorder
Preorder
Postorder

Left subtrees have keys less than the node where it originates
Right subtrees have keys greater than the node where it originates

After inserting, the tree should be balanced
Or only after a certain number of insertions
Or periodically
Or make a copy of the unbalanced tree, balanced it, and then point to the new tree
That way, most insertions will be O(log(n)) and the periodic rebalance will take some seconds

insert = O(n)
list = O(n)
update = O(log(n))
find = O(log(n))

<br>

##### Sorting

Divide and conquer:
Divide the problem into equal part problems
Recursevily solve the subproblems
Combine the results from the subproblems
Stop at indivisible or small inputs

<br>

##### Dynamic programming and memoization

- Store intermediary results
- Do not solve subproblems if they are memoized

Memoization needs recursion
Recursion is expensive, as every function call requires extra memory
What can you memoize? What remains the same in the recursive calls, and what does change?

In dynamic programming, the big problem is divided into smaller subproblems
Each subproblem result is stored, ie, memoized, so it's only calculated once
Dynamic programming is good to solve optimization problems

Steps:
  - Define the base case
  - What decision is made at step n?
  - Dimensions of the memoization array / matrix

<br>

##### Graph algorithms

What is a graph?

How represent them?

Nodes
Edges:
  - Connections between nodes
  - Each edge has a certain weight and characteristics

Adjacency Lsit:
  - A list that contains all nodes
  - Every nodes has all neigbhours explictly defined
  - Each node is defined by a number to be accessed in the list

Adjacency Matrix
  - n by n matrix
  - If there is an edge, then there is a 1 in the matrix

Breadth First Search:

Depth First Search:
  - Does not track distance between nodes well
  - Parents can be tracked

Cycle:
  - When you start and finish in the same node

Directed Graph:
  - Nodes are not bidirectional by default

Graphs can, or not, have weighted edges

Shortest Path:
  - If edges have no weight, perform a BFS
  - 

TODOS:
  - Extract examples from dsa course
  - Create a class for nodes
  - Each node will have an id (fitting a list), a name (optional), neighbours and weights
  - Create a class to represent a graph
  - Use recursion for shortest path and other maybe?
