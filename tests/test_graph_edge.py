import pytest
from src.graph import GraphEdge


def test_simple_edge():
    origin = 1
    dest = 3
    edge = GraphEdge(origin, dest)
    assert str(edge) == f"Edge - Origin: {origin}, Dest: {dest}, Weight: None."
    return


def test_edge_with_weight():
    origin = 1
    dest = 3
    weight = 5
    edge = GraphEdge(origin, dest, weight)
    assert str(edge) == f"Edge - Origin: {origin}, Dest: {dest}, Weight: {weight}."
    return
