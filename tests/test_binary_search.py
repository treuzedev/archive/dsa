from src.binary_search import binary_search
from .helpers.sorted_search_data import test_data


def test_binary_search():
    for test in test_data:
        assert test["expected"] == binary_search(**test["args"])
