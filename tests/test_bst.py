from src.bst import BinarySearchTree
from .helpers.bst_data import UserTest, user_list
from copy import deepcopy
import pytest


def test_find():
    bst = BinarySearchTree()
    for user in user_list:
        bst.insert(bst.root, user.username, user)
    assert bst.find(bst.root, "june").value == user_list[0]
    assert bst.find(bst.root, "nick").value == user_list[6]
    assert bst.find(bst.root, "idontexist") is None


def test_update():
    bst = BinarySearchTree()
    for user in user_list:
        bst.insert(bst.root, user.username, user)
    june = deepcopy(user_list[0])
    june.username = "JUNE"
    bst.update("june", june)
    assert bst.find(bst.root, "june").value.username == "JUNE"
    assert not bst.update("idontexist", june)


def test_setandget():
    bst = BinarySearchTree()
    for user in user_list:
        bst.insert(bst.root, user.username, user)
    moira = deepcopy(user_list[3])
    moira.email = "moira@free.com"
    bst["moira"] = moira
    assert bst["moira"].value.email == "moira@free.com"


def test_size():
    bst = BinarySearchTree()
    for user in user_list:
        bst.insert(bst.root, user.username, user)
    assert bst.size() == 8


def test_height():
    bst = BinarySearchTree()
    for user in user_list:
        bst.insert(bst.root, user.username, user)
    assert bst.height(bst.root) == 5


def test_listall():
    bst = BinarySearchTree()
    for user in user_list:
        bst.insert(bst.root, user.username, user)
    sorted_list = sorted(user_list)
    bst_list = [node.value for node in bst.list_all(bst.root)]
    assert sorted_list == bst_list


def test_isbst():
    bst = BinarySearchTree()
    for user in user_list:
        bst.insert(bst.root, user.username, user)
    false_bst = BinarySearchTree()
    for user in user_list:
        false_bst.insert(false_bst.root, user.username, user)
    false_bst.root.right.left.key = "xxx"
    assert bst.is_bst(bst.root)
    assert not false_bst.is_bst(false_bst.root)


def test_badinsertion():
    bst = BinarySearchTree()
    for user in user_list:
        bst.insert(bst.root, user.username, user)
    with pytest.raises(Exception) as e:
        bst.insert(
            bst.root,
            "moira",
            deepcopy(user_list[3]),
        )
        assert e.value == "Key 'moira' already exists in this tree."


def test_balance():
    bst = BinarySearchTree()
    for user in user_list:
        bst.insert(bst.root, user.username, user)
    assert bst.is_balanced(bst.root) == (False, 5)
    bst.balance_tree(bst.root)
    assert bst.is_balanced(bst.root) == (True, 4)
