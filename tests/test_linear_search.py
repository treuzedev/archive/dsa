from src import linear_search
from .helpers.sorted_search_data import test_data


def test_linear_search():
    for test in test_data:
        assert test["expected"] == linear_search(**test["args"])
