import pytest
from src.graph import Graph, GraphEdge, GraphNode


def setup_nodes():
    return [GraphNode(index=index) for index in range(5)]


def setup_edges():
    edges = [
        (0, 1, 1),
        (1, 0, 1),
        (0, 2, 7),
        (2, 0, 7),
        (0, 3, 2),
        (3, 0, 2),
        (1, 2, 3),
        (2, 1, 3),
        (2, 3, 5),
        (3, 2, 5),
        (2, 4, 1),
        (4, 2, 1),
        (3, 4, 7),
        (4, 3, 7),
    ]
    return [GraphEdge(edge[0], edge[1], weight=edge[2]) for edge in edges]


def setup_graph():
    nodes = setup_nodes()
    edges = setup_edges()
    return Graph(
        nodes=nodes,
        edges=edges,
        max_size=len(nodes),
    )


def test_shortest_path():
    graph = setup_graph()
    distance_1, path_1 = graph.shortest_path(4)
    distance_2, path_2 = graph.shortest_path(4, start=1)
    distance_3, path_3 = graph.shortest_path(0, start=4)
    assert distance_1 == 5
    assert path_1 == [0, 1, 2, 4]
    assert distance_2 == 4
    assert path_2 == [1, 2, 4]
    assert distance_3 == 5
    assert path_3 == [4, 2, 1, 0]
    return
