from src.bubble_sort import bubble_sort
from .helpers.sort_data import test_data
import random


def test_bubble_sort_edges():
    for test in test_data:
        sorted = bubble_sort(**test["args"], in_place=False, logging=True)
        assert sorted == test["expected"]


def test_bubble_sort_10():
    test_10 = list(range(-5, 5))
    test_10_shuffled = list(test_10)
    random.shuffle(test_10_shuffled)
    bubble_sort(test_10_shuffled, logging=True)
    assert test_10_shuffled == test_10


def test_bubble_sort_100():
    test_100 = list(range(-50, 50))
    test_100_shuffled = list(test_100)
    random.shuffle(test_100_shuffled)
    bubble_sort(test_100_shuffled, logging=True)
    assert test_100_shuffled == test_100


def test_bubble_sort_1000():
    test_1000 = list(range(-500, 500))
    test_1000_shuffled = list(test_1000)
    random.shuffle(test_1000_shuffled)
    bubble_sort(test_1000_shuffled, logging=True)
    assert test_1000_shuffled == test_1000


def test_bubble_sort_10000():
    test_10000 = list(range(-5000, 5000))
    test_10000_shuffled = list(test_10000)
    random.shuffle(test_10000_shuffled)
    bubble_sort(test_10000_shuffled, logging=True)
    assert test_10000_shuffled == test_10000
