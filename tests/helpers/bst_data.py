class UserTest:
    def __init__(self, username, email):
        self.username = username
        self.email = email

    def __repr__(self):
        return f"User(username='{self.username}', email='{self.email}')"

    def __str__(self):
        return self.__repr__()

    def __lt__(self, other):
        return self.username < other.username


june = UserTest("june", "june@gilead.com")
lydia = UserTest("lydia", "lydia@gilead.com")
luke = UserTest("luke", "luke@canada.com")
moira = UserTest("moira", "moira@jezebel.com")
thelma = UserTest("thelma", "serena@joy.com")
fred = UserTest("fred", "fred@commander.com")
nick = UserTest("nick", "nick@eyes.com")
god = UserTest("god", "god@nowhere.com")

user_list = [
    june,
    lydia,
    luke,
    moira,
    thelma,
    fred,
    nick,
    god,
]
