test_data = [
    {
        "args": {
            "numbers": [
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
            ],
            "n": 5,
            "search_type": "lazy",
            "logs": True,
        },
        "expected": [4],
    },
    {
        "args": {
            "numbers": [
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
            ],
            "n": 5,
            "search_type": "greedy",
            "logs": True,
        },
        "expected": [4],
    },
    {
        "args": {
            "numbers": [
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
            ],
            "n": 10,
            "search_type": "lazy",
            "logs": True,
        },
        "expected": [],
    },
    {
        "args": {
            "numbers": [
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
            ],
            "n": 10,
            "search_type": "greedy",
            "logs": True,
        },
        "expected": [],
    },
    {
        "args": {
            "numbers": [1, 1, 2, 2, 3, 3, 4, 4, 5],
            "n": 2,
            "search_type": "lazy",
            "logs": True,
        },
        "expected": [2],
    },
    {
        "args": {
            "numbers": [1, 1, 2, 2, 3, 3, 4, 4, 5],
            "n": 3,
            "search_type": "greedy",
            "logs": True,
        },
        "expected": [4, 5],
    },
    {
        "args": {
            "numbers": [n for n in range(0, 10000)] + [9999, 9999],
            "n": 9999,
            "search_type": "lazy",
            "logs": True,
        },
        "expected": [9999],
    },
    {
        "args": {
            "numbers": [n for n in range(0, 10000)] + [9999, 9999],
            "n": 9999,
            "search_type": "greedy",
            "logs": True,
        },
        "expected": [9999, 10000, 10001],
    },
    {
        "args": {
            "numbers": [n for n in range(0, 10000000)] + [9999999, 9999999],
            "n": 9999999,
            "search_type": "lazy",
            "logs": True,
        },
        "expected": [9999999],
    },
    {
        "args": {
            "numbers": [n for n in range(0, 10000000)] + [9999999, 9999999],
            "n": 9999999,
            "search_type": "greedy",
            "logs": True,
        },
        "expected": [9999999, 10000000, 10000001],
    },
    {
        "args": {
            "numbers": [n for n in range(0, 500000)]
            + [n for n in range(499999, 10000000)],
            "n": 499999,
            "search_type": "lazy",
            "logs": True,
        },
        "expected": [499999],
    },
    {
        "args": {
            "numbers": [n for n in range(0, 500000)]
            + [n for n in range(499999, 10000000)],
            "n": 499999,
            "search_type": "greedy",
            "logs": True,
        },
        "expected": [499999, 500000],
    },
]
