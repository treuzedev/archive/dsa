import pytest
from src.graph import Graph, GraphEdge, GraphNode


def setup_simple_nodes():
    return [GraphNode(index=index) for index in range(5)]


def setup_complex_nodes():
    return [
        GraphNode(index=1, name="Albert"),
        GraphNode(index=3, name="Brittany"),
        GraphNode(index=5, name="June"),
        GraphNode(index=7, name="Jon"),
        GraphNode(index=9, name="Plenny"),
    ]


def setup_simple_edges():
    edges = [
        (0, 1),
        (1, 0),
        (0, 4),
        (4, 0),
        (1, 2),
        (2, 1),
        (1, 3),
        (3, 1),
        (1, 4),
        (4, 1),
        (2, 3),
        (3, 2),
        (3, 4),
        (4, 3),
    ]
    return [GraphEdge(edge[0], edge[1]) for edge in edges]


def setup_complex_edges():
    edges = [
        (5, 1, 1),
        (1, 5, 1),
        (5, 7, 2),
        (7, 5, 2),
        (1, 9, 3),
        (9, 1, 3),
        (1, 3, 4),
        (3, 1, 4),
        (1, 7, 5),
        (7, 1, 5),
        (9, 3, 6),
        (3, 9, 6),
        (3, 7, 7),
        (7, 3, 7),
    ]
    return [GraphEdge(edge[0], edge[1], weight=edge[2]) for edge in edges]


def setup_graph_1():
    nodes = setup_simple_nodes()
    edges = setup_simple_edges()
    return Graph(
        nodes,
        edges,
        len(nodes),
    )


def setup_graph_2():
    nodes = setup_complex_nodes()
    edges = setup_complex_edges()
    return Graph(
        nodes,
        edges,
        10,
    )


def test_simple_graph():
    graph = setup_graph_1()
    assert (
        str(graph)
        == "* 0 1 2 3 4\n0 - 1 - - 1\n1 1 - 1 1 1\n2 - 1 - 1 -\n3 - 1 1 - 1\n4 1 1 - 1 -"
    )
    return


def test_complex_graph():
    graph = setup_graph_2()
    assert (
        str(graph)
        == "*        -        Albert   -        Brittany -        June     -        Jon      -        Plenny  \n-        -        -        -        -        -        -        -        -        -        -       \nAlbert   -        -        -        4        -        1        -        5        -        3       \n-        -        -        -        -        -        -        -        -        -        -       \nBrittany -        4        -        -        -        -        -        7        -        6       \n-        -        -        -        -        -        -        -        -        -        -       \nJune     -        1        -        -        -        -        -        2        -        -       \n-        -        -        -        -        -        -        -        -        -        -       \nJon      -        5        -        7        -        2        -        -        -        -       \n-        -        -        -        -        -        -        -        -        -        -       \nPlenny   -        3        -        6        -        -        -        -        -        -       "
    )
    return
