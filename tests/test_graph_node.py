import pytest
from src.graph import GraphNode


def test_simple_graph_node():
    index = 0
    node = GraphNode(index)
    assert node.index == index
    assert not node.name
    assert str(node) == f"Node - Index: {index}, Name: None."
    return


def test_graph_node_with_name():
    index = 0
    name = "doublethink"
    node = GraphNode(index, name)
    assert node.index == index
    assert node.name == name
    assert str(node) == f"Node - Index: {index}, Name: {name}."
    return
