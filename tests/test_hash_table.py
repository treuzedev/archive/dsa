from src.hash_table import HashTable
import itertools as it
import pytest


def test_constructor():
    table_1 = HashTable()
    table_2 = HashTable(max_size=1024)
    assert table_1.max_size == 4096
    assert table_2.max_size == 1024
    return


def test_set_and_get():
    table = HashTable()
    table["iexist"] = "avalue"
    assert table["iexist"] == "avalue"
    assert table["imnone"] is None
    return


def test_update():
    table = HashTable()
    table["imgoingtobeupdated"] = "imtemporary"
    assert table["imgoingtobeupdated"] == "imtemporary"
    table["imgoingtobeupdated"] = "imfinal"
    assert table["imgoingtobeupdated"] == "imfinal"
    return


def test_set_extra_indexes():
    table = HashTable()
    for i in range(table.max_size):
        table[f"{i}"] = i
    with pytest.raises(Exception) as e:
        i += 1
        table[f"{i}"] = i
    assert str(e.value) == "This hash table has no more space left."
    return


def test_same_hash_value():
    table = HashTable()
    table["ola"] = "im OLA"
    table["alo"] = "im ALO"
    assert table["ola"] == "im OLA"
    assert table["alo"] == "im ALO"
    return


def test_length():
    table = HashTable()
    assert len(table) == 0
    for i in range(0, table.max_size, 128):
        table[f"{i}"] = i
    assert len(table) == 32
    return


def test_list_all():
    table = HashTable()
    for i in range(0, table.max_size, 128):
        table[f"{i}"] = i
    assert table.list_all_keys() == set(
        [
            "640",
            "3584",
            "512",
            "3968",
            "1024",
            "1920",
            "384",
            "1152",
            "3200",
            "3712",
            "0",
            "1536",
            "2176",
            "1408",
            "896",
            "1664",
            "3840",
            "128",
            "3072",
            "3456",
            "768",
            "2048",
            "3328",
            "2944",
            "2432",
            "1280",
            "2816",
            "2304",
            "2560",
            "2688",
            "256",
            "1792",
        ]
    )
    return


def test_iter():
    table = HashTable()
    for i in range(0, table.max_size, 128):
        table[f"{i}"] = i
    for key, value in table:
        assert table[key] == int(value)
    return


def test_delete():
    table = HashTable()
    table["imgoingtobedeleted"] = "whyyyyy??"
    table.delete("imgoingtobedeleted")
    assert table["imgoingtobedeleted"] is None
    assert len(table) == 0
    assert table.list_all_keys() == set()
    with pytest.raises(Exception) as e:
        table.delete("idontexist")
    assert str(e.value) == "The specified key, 'idontexist', does not exist."
    return


def test_probing():
    table = HashTable(max_size=32)
    random_chars = [
        "a",
        "b",
        "c",
        "d",
        "e",
        "f",
    ]
    order = sum(ord(x) for x in random_chars)
    combinations_as_tuple = []
    for i in range(len(random_chars)):
        combinations_as_tuple += list(
            it.combinations_with_replacement(
                random_chars,
                (i + 1),
            )
        )
    combinations_as_str = [
        "".join(combination)
        for combination in combinations_as_tuple
        if sum(ord(x) for x in combination) == order
    ]
    for i, entry in enumerate(combinations_as_str):
        table[entry] = i
    for i, entry in enumerate(combinations_as_str):
        assert table[entry] == i
    return
