from src.quicksort import quicksort
from .helpers.sort_data import test_data
import random


def test_quicksort_edges():
    for test in test_data:
        sorted = quicksort(**test["args"], logging=True)
        assert sorted == test["expected"]


def test_quicksort_10():
    test_10 = list(range(-5, 5))
    test_10_shuffled = list(test_10)
    random.shuffle(test_10_shuffled)
    test_10_shuffled = quicksort(test_10_shuffled, logging=True)
    assert test_10_shuffled == test_10


def test_quicksort_100():
    test_100 = list(range(-50, 50))
    test_100_shuffled = list(test_100)
    random.shuffle(test_100_shuffled)
    test_100_shuffled = quicksort(test_100_shuffled, logging=True)
    assert test_100_shuffled == test_100


def test_quicksort_1000():
    test_1000 = list(range(-500, 500))
    test_1000_shuffled = list(test_1000)
    random.shuffle(test_1000_shuffled)
    test_1000_shuffled = quicksort(test_1000_shuffled, logging=True)
    assert test_1000_shuffled == test_1000


def test_quicksort_10000():
    test_10000 = list(range(-5000, 5000))
    test_10000_shuffled = list(test_10000)
    random.shuffle(test_10000_shuffled)
    test_10000_shuffled = quicksort(test_10000_shuffled, logging=True)
    assert test_10000_shuffled == test_10000


def test_quicksort_100000():
    test_100000 = list(range(-50000, 50000))
    test_100000_shuffled = list(test_100000)
    random.shuffle(test_100000_shuffled)
    test_100000_shuffled = quicksort(test_100000_shuffled, logging=True)
    assert test_100000_shuffled == test_100000


def test_quicksort_1000000():
    test_1000000 = list(range(-500000, 500000))
    test_1000000_shuffled = list(test_1000000)
    random.shuffle(test_1000000_shuffled)
    test_1000000_shuffled = quicksort(test_1000000_shuffled, logging=True)
    assert test_1000000_shuffled == test_1000000
