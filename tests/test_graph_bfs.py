import pytest
from src.graph import Graph, GraphEdge, GraphNode


def setup_nodes():
    return [GraphNode(index=index) for index in range(5)]


def setup_edges():
    edges = [
        (0, 1),
        (1, 0),
        (0, 4),
        (4, 0),
        (1, 2),
        (2, 1),
        (1, 3),
        (3, 1),
        (1, 4),
        (4, 1),
        (2, 3),
        (3, 2),
        (3, 4),
        (4, 3),
    ]
    return [GraphEdge(edge[0], edge[1]) for edge in edges]


def setup_graph():
    nodes = setup_nodes()
    edges = setup_edges()
    return Graph(
        nodes=nodes,
        edges=edges,
        max_size=len(nodes),
    )


def test_bfs():
    graph = setup_graph()
    node_1 = graph.bfs(2)
    node_2 = graph.bfs(10)
    node_3 = graph.bfs(0, start=3)
    assert node_1.index == 2
    assert not node_2
    assert node_3.index == 0
    return
