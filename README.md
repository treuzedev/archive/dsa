[![pipeline status](https://gitlab.com/treuzedev/dsa/badges/main/pipeline.svg)](https://gitlab.com/treuzedev/dsa/-/commits/main) [![coverage report](https://gitlab.com/treuzedev/dsa/badges/main/coverage.svg)](https://gitlab.com/treuzedev/dsa/-/commits/main) 

# dsa

Python Data Structures and Algorithms

<br>

## Table of Contents

- Linear Search
- Binary Search
- Binary Search Tree
- Bubble Sort
- Merge Sort
- Quick Sort
- Graph:
  - BFS
  - Shortest Path

<br>

## TODOS

- Insertion Sort (to-do)
- Finish BST coverage tests
- Finish BST delete method

Graph:
- Make sure node indexes are unique
- Make sure edges don't have inexistent node indexes
- More graphs to test
- DFS not done
