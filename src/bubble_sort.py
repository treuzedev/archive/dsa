import time


def bubble_sort(nums, in_place=True, logging=False):
    start_time = time.time()
    if not in_place:
        nums = list(nums)
    counter = 0
    logs = ["---", f"List size: {len(nums)}."]
    nums, counter = sort(nums, counter)
    logs.append("Sort finished!")
    finish_time = time.time()
    delta = finish_time - start_time
    fmt_delta = round(delta, 2) if delta > 1 else round((delta * 1000), 2)
    logs.append(f"Runtime: {fmt_delta}{'s' if delta > 1 else 'ms'}.")
    logs.append(f"Number of iterations: '{counter}'.")
    logs.append("---")
    if logging:
        for line in logs:
            print(line)
    if not in_place:
        return nums
    else:
        return True


def sort(nums, counter):
    flag = True
    while flag:
        flag = False
        for i in range(len(nums) - 1):
            counter += 1
            if nums[i] > nums[(i + 1)]:
                nums[i], nums[(i + 1)] = nums[(i + 1)], nums[i]
                flag = True
    return nums, counter
