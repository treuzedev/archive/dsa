import time


def linear_search(numbers, n, search_type="lazy", logs=False):
    """
    description: A function that returns the first index of a given number using a linear search approach; expects a sorted list of numbers, where the index 0 is the element with the smaller value.
    args:
      required:
        numbers:
          description: A list of integers.
          example: [1, 2, 3]
        n:
          description: The integer to find.
          example: 2
      optional:
        search_type:
          description: A string, indicating if all occurences of n should be returned, or only the first one; only the values 'lazy' and 'greedy' are allowed.
          default: 'lazy'
        logs:
          description: A Boolean, indicating whether or not print statements should be outputed
          default: False
    returns:
      description: A list. If search_type is 'lazy', then the list contains only the first occurence. If search_type is 'greedy', returns a list with all the occurrences of the number specified. An empty list is returned if the number is not found.
      example: 1
    """
    start_time = time.time()
    space = 0
    result = []
    logs = [
        "---",
        f"List size: {len(numbers)}.",
        f"Number to find: {n}.",
        f"Search type: {search_type}.",
    ]
    for index, number in enumerate(numbers):
        space += 1
        if number > n:
            logs.append(
                f"Number at index {index} is bigger then the specified number, {n}. Stopping search."
            )
            break
        elif number == n and search_type == "lazy":
            result.append(index)
            break
        elif number == n and search_type == "greedy":
            result.append(index)
    logs.append("Search finished!")
    finish_time = time.time()
    delta = finish_time - start_time
    fmt_delta = round(delta, 2) if delta > 1 else round((delta * 1000), 2)
    logs.append(f"Runtime: {fmt_delta}{'s' if delta > 1 else 'ms'}.")
    logs.append(f"Number of iterations: {space}.")
    logs.append(f"Indexes found: {result}")
    logs.append("---")
    for line in logs:
        print(line)
    return result
