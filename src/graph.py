class GraphNode:
    def __init__(
        self,
        index,
        name=None,
    ):
        self.index = index
        self.name = name

    def __repr__(self):
        return f"Node - Index: {self.index}, Name: {self.name}."

    def __str__(self):
        return repr(self)


class GraphEdge:
    def __init__(
        self,
        origin,
        dest,
        weight=None,
    ):
        self.origin = origin
        self.dest = dest
        self.weight = weight

    def __repr__(self):
        return (
            f"Edge - Origin: {self.origin}, Dest: {self.dest}, Weight: {self.weight}."
        )

    def __str__(self):
        return repr(self)


class Graph:
    def __init__(
        self,
        nodes,
        edges,
        max_size,
    ):
        self.max_size = max_size
        self.save_nodes(nodes)
        self.set_adjacency_as_list(edges)
        self.set_adjacency_as_matrix(edges)
        return

    def __repr__(self):
        matrix_repr = self.get_matrix_repr()
        for row in matrix_repr:
            for index, el in enumerate(row):
                row[index] = str(el)
                while len(row[index]) < self.biggest_name_len:
                    row[index] += " "
        return "\n".join(" ".join(str(n) for n in row) for row in matrix_repr)

    def get_matrix_repr(self):
        nodes_as_row = []
        nodes_as_columns = [["*"]]
        for index in range(self.max_size):
            node = self.nodes.get(index)
            node_value = node.name or node.index if node else "-"
            nodes_as_row.append(node_value)
            nodes_as_columns += [[node_value]]
        matrix_repr = [nodes_as_row] + self.data_as_matrix
        return [nodes_as_columns[index] + row for index, row in enumerate(matrix_repr)]

    def __str__(self):
        return repr(self)

    def save_nodes(self, nodes):
        self.nodes = {}
        self.biggest_name_len = 0
        for node in nodes:
            self.nodes[node.index] = node
            if node.name and len(node.name) > self.biggest_name_len:
                self.biggest_name_len = len(node.name)
        return

    def set_adjacency_as_list(self, edges):
        self.data_as_list = [[] for _ in range(self.max_size)]
        for edge in edges:
            self.data_as_list[edge.origin] += [edge.dest]
        return

    def set_adjacency_as_matrix(self, edges):
        self.data_as_matrix = [
            ["-" for _ in range(self.max_size)] for _ in range(self.max_size)
        ]
        for edge in edges:
            self.data_as_matrix[edge.origin][edge.dest] = (
                1 if not edge.weight else edge.weight
            )
        return

    def bfs(self, target, start=0):
        node, queue, visited = None, [start], set()
        while queue:
            el = queue.pop(0)
            if el not in visited:
                if self.nodes[el].index == target:
                    node = self.nodes[el]
                    break
                else:
                    queue += self.data_as_list[el]
                visited.add(el)
        return node

    def shortest_path(self, target, start=0):
        queue, visited = [start], set()
        distances = self.get_initial_distances(start)
        parents = self.get_initial_parents()
        while queue:
            el = queue.pop(0)
            visited.add(el)
            neighbours = self.data_as_list[el]
            distances, parents = self.visit_neighbours(
                el, neighbours, distances, visited, parents
            )
            next_node = self.pick_next_node(distances, visited)
            if next_node or next_node == 0:
                queue.append(next_node)
        return distances[target], self.get_path(parents, target)

    def get_initial_distances(self, start):
        distances = {node_index: float("inf") for node_index in self.nodes}
        distances[start] = 0
        return distances

    def get_initial_parents(self):
        return {node_index: None for node_index in self.nodes}

    def visit_neighbours(self, el, neighbours, distances, visited, parents):
        for n in neighbours:
            if n not in visited:
                w = self.data_as_matrix[el][n]
                cost = w + distances[el]
                if cost < distances[n]:
                    distances[n] = cost
                    parents[n] = el
        return distances, parents

    def pick_next_node(self, distances, visited):
        next_node = None
        next_node_value = float("inf")
        for node, node_value in distances.items():
            if node not in visited and node_value < next_node_value:
                next_node = node
                next_node_value = node_value
        return next_node

    def get_path(self, parents, target):
        path = []
        node = target
        while node or node == 0:
            path.insert(0, node)
            node = parents[node]
        return path
