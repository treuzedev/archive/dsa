class HashTable:
    def __init__(self, max_size=4096):
        self.max_size = max_size
        self.__keys = [None] * self.max_size
        self.__keys_as_set = set()
        self.__size = 0
        return

    def __setitem__(self, key, value):
        self.__update(key, value)
        return

    def __getitem__(self, key):
        return self.__find(key)

    def __iter__(self):
        return (x for x in self.__keys if x is not None)

    def __len__(self):
        return self.__size

    def list_all_keys(self):
        return self.__keys_as_set

    def __update(self, key, value):
        if key in self.__keys_as_set:
            index = self.__get_index(key)
        else:
            index = self.__set_index(key)
            self.__keys_as_set.add(key)
            self.__size += 1
        self.__keys[index] = (key, value)
        return

    def __find(self, key):
        if key not in self.__keys_as_set:
            return None
        index = self.__get_index(key)
        return self.__keys[index][1]

    def delete(self, key):
        if key not in self.__keys_as_set:
            raise ValueError(f"The specified key, '{key}', does not exist.")
        index = self.__get_index(key)
        self.__keys[index] = None
        self.__keys_as_set.remove(key)
        self.__size -= 1
        return

    def __get_index(self, key):
        hashed_key = self.__hash_function(key)
        return self.__get_linear_probing(hashed_key, key)

    def __set_index(self, key):
        hashed_key = self.__hash_function(key)
        return self.__set_linear_probing(hashed_key)

    def __hash_function(self, key):
        counter = sum(ord(char) for char in key)
        while counter > self.max_size:
            counter %= ord(key[0])
        return counter

    def __set_linear_probing(self, hashed_key):
        if self.__size >= self.max_size:
            raise ValueError("This hash table has no more space left.")
        for i in range(hashed_key, self.max_size):
            if self.__keys[i] is None:
                return i
        for i in range(0, hashed_key):
            if self.__keys[i] is None:
                return i

    def __get_linear_probing(self, hashed_key, key):
        for i in range(hashed_key, self.max_size):
            if self.__keys[i][0] == key:
                return i
        for i in range(0, hashed_key):
            if self.__keys[i][0] == key:
                return i
