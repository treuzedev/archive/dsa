class Node:
    def __init__(self, key, value=None):
        self.key = key
        self.value = value
        self.parent = None
        self.left = None
        self.right = None


class BinarySearchTree:
    def __init__(self):
        self.root = None
        self._treesize = 0

    def __repr__(self):
        pass

    def __str__(self):
        pass

    def __setitem__(self, key, value):
        node = self.find(self.root, key)
        if node:
            self.update(key, value)
        else:
            self.insert(node, key, value)
            if not self.is_balanced(self.root):
                self.balance_tree(self.root)

    def __getitem__(self, key):
        return self.find(self.root, key)

    def __iter__(self):
        return iter(self.list_all(self.root))

    def height(self, node, h=0):
        if node is None:
            return h
        h += 1
        left_heigh = self.height(node.left, h=h)
        right_height = self.height(node.right, h=h)
        return max(left_heigh, right_height)

    def size(self):
        return self._treesize

    def display(self, node, level=0):
        space = "\t"
        if node is None:
            print((space * level), "NONE")
            return True
        if node.left is None and node.right is None:
            print((space * level), node.key)
            return True
        self.display(node.right, level=(level + 1))
        print((space * level), node.key)
        self.display(node.left, level=(level + 1))

    def is_bst(self, node):
        if node is None:
            return True
        if node.right and node.right.key < node.key:
            return False
        if node.left and node.left.key > node.key:
            return False
        return bool(self.is_bst(node.right) and self.is_bst(node.left))

    def is_balanced(self, node):
        if node is None:
            return True, 0
        left_balance, left_height = self.is_balanced(node.left)
        right_balance, right_height = self.is_balanced(node.right)
        height = max(left_height, right_height) + 1
        if (
            left_balance
            and right_balance
            and abs(left_height - right_height) > 1
            or not left_balance
            or not right_balance
        ):
            return False, height
        else:
            return True, height

    def make_balanced_bst(self, nodes, low, high):
        if low > high:
            return True
        m = (low + high) // 2
        self.insert(self.root, nodes[m].key, nodes[m])
        self.make_balanced_bst(nodes, low, (m - 1))
        self.make_balanced_bst(nodes, (m + 1), high)
        return True

    def balance_tree(self, node):
        nodes = self.list_all(node)
        self.root = None
        self.make_balanced_bst(
            nodes,
            0,
            (len(nodes) - 1),
        )
        return True

    def insert(self, node, key, value):
        tmp = self.find(self.root, key)
        if tmp:
            raise ValueError(f"Key '{key}' already exists in this tree.")
        if node is None:
            node = Node(key, value)
            self._treesize += 1
        if self.root is None:
            self.root = node
            return True
        elif key > node.key:
            node.right = self.insert(node.right, key, value)
            node.right.parent = node
        elif key < node.key:
            node.left = self.insert(node.left, key, value)
            node.left.parent = node
        return node

    def find(self, node, key):
        if node is None:
            return None
        elif key == node.key:
            return node
        elif key > node.key:
            return self.find(node.right, key)
        elif key < node.key:
            return self.find(node.left, key)

    def update(self, key, value):
        old = self.find(self.root, key)
        if old is not None:
            old.value = value
            return True
        return False

    def delete(self, key):
        self._treesize -= 1
        return True

    def list_all(self, node):
        if node is None:
            return []
        return self.list_all(node.left) + [node] + self.list_all(node.right)
