import time
import random


def quicksort(nums, logging=False):
    start_time = time.time()
    logs = [
        "---",
        f"List size: {len(nums)}.",
    ]
    sort(nums, 0, (len(nums) - 1))
    logs.append("Sort finished!")
    finish_time = time.time()
    delta = finish_time - start_time
    fmt_delta = round(delta, 2) if delta > 1 else round((delta * 1000), 2)
    logs.append(f"Runtime: {fmt_delta}{'s' if delta > 1 else 'ms'}.")
    logs.append("---")
    if logging:
        for line in logs:
            print(line)
    return nums


def sort(nums, start, end):
    if start < end:
        pivot = partition(
            nums,
            start,
            end,
        )
        sort(
            nums,
            start,
            (pivot - 1),
        )
        sort(
            nums,
            (pivot + 1),
            end,
        )
    return nums


def get_pivot(start, end):
    return random.choice(
        range(
            start,
            (end + 1),
        )
    )


def partition(nums, start, end):
    pivot = get_pivot(
        start,
        end,
    )
    nums[pivot], nums[end] = nums[end], nums[pivot]
    left, right = start, (end - 1)
    while right > left:
        if nums[left] <= nums[end]:
            left += 1
        elif nums[right] > nums[end]:
            right -= 1
        else:
            nums[left], nums[right] = nums[right], nums[left]
    if nums[left] > nums[end]:
        nums[left], nums[end] = nums[end], nums[left]
        return left
    else:
        return end
