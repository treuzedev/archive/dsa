import time


def merge_sort(nums, logging=False):
    start_time = time.time()
    logs = [
        "---",
        f"List size: {len(nums)}.",
    ]
    nums = sort(nums)
    logs.append("Sort finished!")
    finish_time = time.time()
    delta = finish_time - start_time
    fmt_delta = round(delta, 2) if delta > 1 else round((delta * 1000), 2)
    logs.append(f"Runtime: {fmt_delta}{'s' if delta > 1 else 'ms'}.")
    logs.append("---")
    if logging:
        for line in logs:
            print(line)
    return nums


def sort(nums):
    l = len(nums)
    if l <= 1:
        return nums
    m = l // 2
    left, right = sort(nums[:m]), sort(nums[m:])
    return merge(left, right)


def merge(left, right):
    sorted = []
    i, j = 0, 0
    while i < len(left) and j < len(right):
        if left[i] <= right[j]:
            sorted.append(left[i])
            i += 1
        else:
            sorted.append(right[j])
            j += 1
    return sorted + left[i:] + right[j:]
