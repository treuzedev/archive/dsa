import time


def binary_search(numbers, n, search_type="lazy", logs=False):
    """
    description: A function that returns the first index of a given number using a binary search approach; expects a sorted list of numbers, where the index 0 is the element with the smaller value.
    args:
      required:
        numbers:
          description: A list of integers.
          example: [1, 2, 3]
        n:
          description: The integer to find.
          example: 2
      optional:
        search_type:
          description: A string, indicating if all occurences of n should be returned, or only the first one; only the values 'lazy' and 'greedy' are allowed.
          default: 'lazy'
        logs:
          description: A Boolean, indicating whether or not print statements should be outputed
          default: False
    returns:
      description: A list. If search_type is 'lazy', then the list contains only the first occurence. If search_type is 'greedy', returns a list with all the occurrences of the number specified. An empty list is returned if the number is not found.
      example: 1
    """
    start_time = time.time()
    space = 0
    logs = ["---"]
    logs.append(f"List size: {len(numbers)}.")
    logs.append(f"Number to find: {n}.")
    logs.append(f"Search type: {search_type}.")
    result, space = binary_locate(numbers, n, space)
    if result:
        result, space = search_for_neighbors(numbers, space, result, search_type)
    logs.append("Search finished!")
    finish_time = time.time()
    delta = finish_time - start_time
    fmt_delta = round(delta, 2) if delta > 1 else round((delta * 1000), 2)
    logs.append(f"Runtime: {fmt_delta}{'s' if delta > 1 else 'ms'}.")
    logs.append(f"Number of iterations: {space}.")
    logs.append(f"Indexes found: {result}")
    logs.append("---")
    for line in logs:
        print(line)
    if search_type == "greedy":
        return result
    elif search_type == "lazy":
        if result:
            return [result[0]]
        else:
            return []


def binary_locate(numbers, n, space):
    h = len(numbers) - 1
    l = 0
    while l <= h:
        space += 1
        m = int((h + l) / 2)
        if numbers[m] == n:
            return [m], space
        elif numbers[m] > n:
            h = m - 1
        elif numbers[m] < n:
            l = m + 1
    return [], space


def search_for_neighbors(numbers, space, result, search_type):
    m = result[0]
    for i in range((m - 1), -1, -1):
        space += 1
        if numbers[i] == numbers[m]:
            result.insert(0, i)
        elif numbers[i] < numbers[m]:
            break
    if search_type == "greedy":
        for i in range((m + 1), len(numbers)):
            space += 1
            if numbers[i] == numbers[m]:
                result.append(i)
            elif numbers[i] > numbers[m]:
                break
    return result, space
