class User(a, b, c):
    def __init__():
        pass
    def __repr__():
        pass
    def __str__():
        pass
    def __setitem__():
        # use user['someposition'] = a_user
        # instead of user.insert(a_user)
        pass
    def __getitem__():
        pass
    def __iter__():
        # use the class in a for loop
        pass
    def __len__():
        # use len()
        pass
    

class TreeNode():
    def __init__(self, key):
        self.key = key
        self.right = None
        self.left = None

class BSTNode():
    def __init__(self, key, value=None):
        self.key = key
        self.value = value
        self.parent = None
        self.right = None
        self.left = None
